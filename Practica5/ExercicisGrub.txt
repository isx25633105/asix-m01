﻿Joan Almirall Canuto
isx2563105
Maig 2020
Practica 5:


1. Múltiples menús
● Practicar l’arrencada amb els següents timeouts: 5, 0 i -1.
Modifiquem el grup les dues entrades de timeout cambiant per 5, 0 i -1. En el moment que posem 5 i rebotem tindrem 5 segons per escollir la entrada. Quan cambiem per 0 segons i rebotem ens entrara directament a per defecte. Quan cambiem per -1 i rebotem tindrem tot el temps del món per escollir qualsevol de les entrades.
● Practicar l’arrencada modificant el amb set default amb els valors 0 i 2.
Si posem al grup el set_default en 0 tiendrem tot el temps que volguem en entrar a alguna de les entrades, en cambi si posem 2 automaticament ens entrara a la 2.
 
● Crear el menú:
set default=0
set timeout=-1
menuentry 'Fedora normal' --class fedora --class gnu-linux --class gnu --class os --unrestricted $menuentry_id_option 'gnulinux-4.18.19-100.fc27.x86_64-advanced-22f893a2-8b36-476e-b6c6-1c42fddc4349' {
        insmod gzio
        insmod part_msdos
        insmod ext2
        set root='hd0,msdos1'
        linux16 /vmlinuz-4.18.19-100.fc27.x86_64 root=/dev/mapper/fedora-root ro rd.lvm.lv=fedora/root rd.lvm.lv=fedora/swap quiet
        initrd16 /initramfs-4.18.19-100.fc27.x86_64.img
}

menuentry 'Fedora despullat' --class fedora --class gnu-linux --class gnu --class os --unrestricted {
        insmod gzio
        insmod part_msdos
        insmod ext2
        set root='hd0,msdos1'
        linux16 /vmlinuz-4.18.19-100.fc27.x86_64 root=/dev/mapper/fedora-root ro rd.lvm.lv=fedora/root rd.lvm.lv=fedora/swap quiet init=/bin/bash
        initrd16 /initramfs-4.18.19-100.fc27.x86_64.img
}

menuentry 'Fedora rescat' --class fedora --class gnu-linux --class gnu --class os --unrestricted {
        insmod gzio
        insmod part_msdos
        insmod ext2
        set root='hd0,msdos1'
        linux16 /vmlinuz-4.18.19-100.fc27.x86_64 root=/dev/mapper/fedora-root ro rd.lvm.lv=fedora/root rd.lvm.lv=fedora/swap quiet systemd.unit=rescue.target
        initrd16 /initramfs-4.18.19-100.fc27.x86_64.img
}

menuentry 'Fedora emergencia' --class fedora --class gnu-linux --class gnu --class os --unrestricted {
        insmod gzio
        insmod part_msdos
        insmod ext2
        set root='hd0,msdos1'
        linux16 /vmlinuz-4.18.19-100.fc27.x86_64 root=/dev/mapper/fedora-root ro rd.lvm.lv=fedora/root rd.lvm.lv=fedora/swap quiet systemd.unit=emergency.target
        initrd16 /initramfs-4.18.19-100.fc27.x86_64.img
}


2. Submenús
● Establir el timeout de manera que esperi una selecció i definir la primera opció de
menú com a opció per defecte.
Grub:
set_default= 1
set_timeout=-1
● Generar el menú:
Grub:
set_default= 1
set_timeout=-1

menuentry 'Fedora normal' --class fedora --class gnu-linux --class gnu --class os --unrestricted $menuentry_id_option 'gnulinux-4.18.19-100.fc27.x86_64-advanced-22f893a2-8b36-476e-b6c6-1c42fddc4349' {
        insmod gzio
        insmod part_msdos
        insmod ext2
        set root='hd0,msdos1'
        linux16 /vmlinuz-4.18.19-100.fc27.x86_64 root=/dev/mapper/fedora-root ro rd.lvm.lv=fedora/root rd.lvm.lv=fedora/swap quiet
        initrd16 /initramfs-4.18.19-100.fc27.x86_64.img
}
submenu 'Fedora Secundaris' {
menuentry 'Fedora despullat' --class fedora --class gnu-linux --class gnu --class os --unrestricted {
        insmod gzio
        insmod part_msdos
        insmod ext2
        set root='hd0,msdos1'
        linux16 /vmlinuz-4.18.19-100.fc27.x86_64 root=/dev/mapper/fedora-root ro rd.lvm.lv=fedora/root rd.lvm.lv=fedora/swap quiet init=/bin/bash
        initrd16 /initramfs-4.18.19-100.fc27.x86_64.img
}

menuentry 'Fedora rescat' --class fedora --class gnu-linux --class gnu --class os --unrestricted {
        insmod gzio
        insmod part_msdos
        insmod ext2
        set root='hd0,msdos1'
        linux16 /vmlinuz-4.18.19-100.fc27.x86_64 root=/dev/mapper/fedora-root ro rd.lvm.lv=fedora/root rd.lvm.lv=fedora/swap quiet systemd.unit=rescue.target
        initrd16 /initramfs-4.18.19-100.fc27.x86_64.img
}

menuentry 'Fedora emergencia' --class fedora --class gnu-linux --class gnu --class os --unrestricted {
        insmod gzio
        insmod part_msdos
        insmod ext2
        set root='hd0,msdos1'
        linux16 /vmlinuz-4.18.19-100.fc27.x86_64 root=/dev/mapper/fedora-root ro rd.lvm.lv=fedora/root rd.lvm.lv=fedora/swap quiet systemd.unit=emergency.target
        initrd16 /initramfs-4.18.19-100.fc27.x86_64.img
}
}

menuentry 'Fedora rescat' --class fedora --class gnu-linux --class gnu --class os --unrestricted {
        insmod gzio
        insmod part_msdos
        insmod ext2
        set root='hd0,msdos1'
        linux16 /vmlinuz-4.18.19-100.fc27.x86_64 root=/dev/mapper/fedora-root ro rd.lvm.lv=fedora/root rd.lvm.lv=fedora/swap quiet systemd.unit=rescue.target
        initrd16 /initramfs-4.18.19-100.fc27.x86_64.img
}

3. Mode editar entrada: bàsic
● Editar l’entrada per defecte afegint un 1 al final per entrar en mode rescat.

menuentry 'Fedora Rescat' --class fedora --class gnu-linux --class gnu --class os --unrestricted $menuentry_id_option 'gnulinux-4.18.19-100.fc27.x86_64-advanced-22f893a2-8b36-476e-b6c6-1c42fddc4349' {
        insmod gzio
        insmod part_msdos
        insmod ext2
        set root='hd0,msdos1'
        linux16 /vmlinuz-4.18.19-100.fc27.x86_64 root=/dev/mapper/fedora-root ro rd.lvm.lv=fedora/root rd.lvm.lv=fedora/swap quiet 1
        initrd16 /initramfs-4.18.19-100.fc27.x86_64.img
}

Editar l’entrada per defecte afegint systemd.unit=rescue.target al final.

menuentry 'Fedora rescue.target' --class fedora --class gnu-linux --class gnu --class os --unrestricted $menuentry_id_option 'gnulinux-4.18.19-100.fc27.x86_64-advanced-22f893a2-8b36-476e-b6c6-1c42fddc4349' {
        insmod gzio
        insmod part_msdos
        insmod ext2
        set root='hd0,msdos1'
        linux16 /vmlinuz-4.18.19-100.fc27.x86_64 root=/dev/mapper/fedora-root ro rd.lvm.lv=fedora/root rd.lvm.lv=fedora/swap quiet systemd.unit=rescue.target
        initrd16 /initramfs-4.18.19-100.fc27.x86_64.img
}

Editar l’entrada per defecte i esborrar totes les línies. Tornar-les a escriure només les
bàsiques necessàries per a una entrada ‘despullada’.

menuentry 'Fedora basico' --class fedora --class gnu-linux --class gnu --class os --unrestricted $menuentry_id_option 'gnulinux-4.18.19-100.fc27.x86_64-advanced-22f893a2-8b36-476e-b6c6-1c42fddc4349' {
        insmod gzio
        insmod part_msdos
        insmod ext2
        set root='hd0,msdos1'
        linux16 /vmlinuz-4.18.19-100.fc27.x86_64 root=/dev/mapper/fedora-root ro rd.lvm.lv=fedora/root rd.lvm.lv=fedora/swap quiet init=/bin/bash
        initrd16 /initramfs-4.18.19-100.fc27.x86_64.img
}



4. Generar menús automàticameny: grub2-mkconfig
● Usant l’ordre ​ grub2-mkconfig mostrar per stdout el menú de grub2 que es genera
automàticament amb aquesta eina.
Quan exeutem grub2-mkconfig es generea un nou fitxer de configuració del grup.

● Fer el mateix generant el fitxer grub.conf.out usant un redireccionament de sortida.
grub2-mkconfig > grub.conf.out

● Desar una còpia de seguretat del fitxer del menú de grub actual ​ grub.conf
anomenant-la ​ grub.unso.conf . 
cp grub.cfg grub.unso.conf

● Generar un nou fitxer de menú automatitzadament, un nou ​ grub.conf . 
grub2-mkconfig > grub.cfg

● Verificar l’arrencada.
Si faig reboot em surten totes les meves entrades, tinc un Fedora 27, un centos 7 i un devian. Tots 3 surten amb les seves opcions avançades aixi que ha funcionat.

5. Mode Comanda: configfile / llistar
● Des del mode comanda usar l’ordre ​ configfile per seleccionar quin dels fitxers de
menú de grub volem engegar:
grub > configfile (hd0,msdos1)/gru2/grub.conf.out

● Engegar el sistema usant el menú grub.conf.
 grub> configfile (hd0,msdos1)/grub2/grub.conf
Reboot

En mode comanda usant l’ordre configfile engegar el sistema utilitzant el menú
grub.unso.conf.
grub> configfile (hd0,msdos1)/grub2/grub.unso.conf

6. Instal·lar un nou sistema: ara qui mana?
Normalment el nou es el que mana i no conserva el antic modificat. (Si es un windows destrueix  el grub).

7. Establir qui mana (1): local
Instalar un sistema operatiu poden pasar diverses coses. Si instales windows segúr que destrueix el grub. Si instal.les devian i no desmarques que instal.li el seu grub posa el grub de devian. Si tu instales un sistema operatiu que no elimina el grub, de primeres no et surtira una entrada, hauras de tornar a configurar grub2-mkconfig per que ho detecti la nova entrada si no funciona hauras de fer-ho manualment.

8. Iniciar un sistema operatiu no llistat (1): editar una entrada
Aixì es fa si no et detecta la partició el grub
manualment, edites una entrada per defete per entrar al altre sistema operatiu i la edites al grup
'Devian'
        load_video
        set gfxpayload=keep
        insmod gzio
        insmod part_msdos
        insmod ext2
        set root='hd0,msdos4'
        linux16 /boot/vmlinuz-4,19.0-9-amd64 root=/dev/vda2 ro quiet
        initrd16 /boot/initrd.img-4.19.0-9-amd64
Ara funciona i a més puc accedir al meu devian.
9. Iniciar un sistema operatiu no llistat (2): usar configfile
grub> configfile (hd0,msdos4)/boot/grub2/grub.cfg

10. Establir qui mana (2): un altre device
Auriem de iniciar una partició muntar una altre a /mnt i executar el grub2-install per fer que l’altre fos la que te el control.
11. Password (1): establir un password a una partició
Per posar una password es necesita un usuari.
Per exemple nomes isx25633105
Al grup del menu:
set users=”isx25633105”
password isx25633105  123456777

Al meuntri cambiem el unrestricted per  --user isx25633105

* Forma més fàcil que nomes entri root.
Al meuntri posem   –user “ ”
12. Password (2): establir un password a un grub dual.
Semblant a el anterior tornar a treure unrestricted.
Aixè nomes root podria accedir a el grub global i com que te una contreseña no fa falta crearla. 
-user “ “

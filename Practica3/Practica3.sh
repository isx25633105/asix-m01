#!/bin/bash
# Joan Almirall
# isx25633105
# Abril 2020
#-------------------------------------------

# 1.
function fsize(){
  usuario=$1
  home=$(grep "^$usuario:" /etc/passwd | cut -d: -f6)
  longitud=$(du -s $home 2> /dev/null)
  echo $longitud
  return 0
}

# 2.
function loginargs(){

  # Errores
  ERR_NARGS=1
  ERR_NOLOGIN=2

  # Validar argumento
  if [ $# -lt 1 ]; then
    echo "ERROR:Nombre de argumentos  incorrectos. Ha de ser almenos uno." >> /dev/stderr 
    echo "USAGE:loginargs login[...]" >> /dev/stderr
    return $ERR_NARGS
  fi

  #Xixa
  salida=0
  listarlogins=$*
  for usuario in $listarlogins; 
  do
    # V.logins
    grep "^$usuario:" /etc/passwd &> /dev/null
    if [ $? -ne 0 ]; then
      echo "ERROR: el login $usuario no existe" >> /dev/stderr
      echo "USAGE:loginargs login[...]" >> /dev/stderr
      sortida=$ERR_NOLOGIN
    else
      echo "$usuario:"
      fsize $usuario
    fi
  done
  return $salida
}

#3.
function loginfile(){

  # Errores
  ERR_NARGS=1
  ERR_FILE=2
  ERR_NOLOGIN=3

  # Validar argumento
  if [ $# -ne 1 ]; then
    echo "ERROR:Nombre d'argumentos incorrectos. Ha de ser uno." >> /dev/stderr
    echo "USAGE:loginfile archivo" >> /dev/stderr
    return $ERR_NARGS
  fi

  # V. Archivo
  if ! [ -f $1 ]; then
    echo "ERROR:$1 no és un regular file." >> /dev/stderr
    echo "USAGE:loginfile archivo." >> /dev/stderr
    return $ERR_FILE
  fi

  # Xixa
  salida=0
  fileIn=$1
  while read -r usuario; 
  do
    # V.login
    grep "^$usuario:" /etc/passwd &> /dev/null
    if [ $? -ne 0 ]; then
      echo "ERROR: el login $usuario no se encuentra en el sis" >> /dev/stderr
      echo "USAGE:loginargs login[...]" >> /dev/stderr
      salida=$ERR_NOLOGIN
    else
      echo "$usuario:"
      fsize $usuario
    fi
  done < $fileIn
  return $salida
}

# 4.

function loginboth(){
  
  # Errores
  ERR_ARGS=1
  ERR_FILE=2
  ERR_LOGIN=3
  
  # Validar Argumentos
  if [ $# -gt 1 ]; then
    echo "ERROR: Numero de argumentos invalida. Ha de ser uno" >> /dev/stderr
    echo "USAGE: loginboth [archivo]" >> /dev/stderr
    return $ERR_ARGS
  fi

  # Xixa
  salida=0
  fileIn=/dev/stdin

  if [ $# -eq 1 ]; then
    # V.File
    if ! [ -f $1 ];then
      echo "ERROR: $1 no es un regular file." >> /dev/stderr
      echo "USAGE: loginboth [archivo]" >> /dev/stderr
      return $ERR_FILe
    fi
      fileIn=$1
  fi

  while read -r usuario; 
  do
    # V.Usuario
    grep "^$usuario:" /etc/passwd &> /dev/null
    if [ $? -ne 0 ]; then
      echo "ERROR: el usuario  $usuario no se encuentra en sys." >> /dev/stderr
      echo "USAGE: loginboth [archivo]" >> /dev/stderr
      salida=$ERR_LOGIN
    
    else
      echo "$usuario"
      fsize $usuario
    fi
  done < $fileIn
  return $salida
}

# 5.
function grepgid(){

  # Errores
  ERR_ARGS=1
  ERR_GID=2

  # Validar Argumentos
  if [ $# -ne 1 ]; then
    echo "ERROR: Numero de argumentos invalidos. Ha de ser uno." >> /dev/stderr
    echo "USAGE: gid." >> /dev/stderr
    return $ERR_ARGS
  fi

  # V.Gid
  grep "^[^:]*:[^:]*:$1:" /etc/group &> /dev/null
  if [ $? -ne 0 ]; then
    echo "ERROR: El grupo $1 no se encuentra en sys." >> /dev/stderr
    echo "USAGE: gid." >> /dev/stderr
    return $ERR_GID
  fi

  # Xixa
  gid=$1
  grep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | cut -d: -f1
  return 0
}

# 6.

function gidsize(){

  # Errores
  ERR_ARGS=1
  ERR_GID=2
  
  # Validar Argumentos
  if [ $# -ne 1 ]; then
    echo "ERROR: Numero de argumentos invalido. Ha de ser uno." >> /dev/stderr
    echo "USAGE: +gid"
    return $ERR_ARGS
  fi

  # V.Gid
  grep "^[^:]*:[^:]*:$1:" /etc/group &> /dev/null
  if [ $? -ne 0 ]; then
    echo "ERROR: El grupo $1 no se encuentra en el sys." >> /dev/stderr
    echo "USAGE: grepgid." >> /dev/stderr
    return $ERR_ARGS
  fi
  
  # Xixa
  gid=$1
  listaDeMiembros=$(grepgid $gid)
  for usuario in $listaDeMiembros; 
  do
    fsize $usuario
  done
  return 0
}

# 7.
function allgidsize(){

  
  listadodegids=$(cut -d: -f3 /etc/group | sort -g)
  for gid in $listadodegids; do
    echo "$gid"
    gidsize $gid
    echo
  done
  return 0
}

# 8.
function allgroupsize(){
  while read -r linia; 
  do
    gid=$(echo $linia | cut -d: -f3)
    if [ $gid -ge 0 -a $gid -le 100 ];then
      echo $linia
      gidsize $gid
    else
      return 0
    fi
  done < /etc/group
  return 0
}


# 9. 

function fstype(){
  fstype=$1
  fstabbueno=$(egrep -v "^#|^$" /etc/fstab | tr -s [:blank:] ':')
  echo "$fstabbueno" | grep "^[^:]*:[^:]*:$fstype:" | cut -d: -f1,2 | sort | tr ':' '\t'
  return 0
}

# 10. 
function allfstype(){

  fstabbueno=$(egrep -v "^#|^$" /etc/fstab | tr -s [:blank:] ':')
  llistafstypeordenada=$(echo "$fstabbueno" | cut -d: -f3 | sort -u)
  for distintos in $llistafstypeordenada; do
    fstype $distintos
  done
  return 0

}

# 11.
function allfstypeif(){
  #Validar minimo
  minimo=$1
  fstabbueno=$(egrep -v "^#|^$" /etc/fstab | tr -s [:blank:] ':')
  numerolineasordenados=$(echo "$fstabbueno" | wc -l)
  if [ "$nombreLineasordenados" -lt "$minimo" ]; then
    echo "ERROR: Numero de entradas menores al $minimo." >> /dev/stderr
    echo "USAGE: minim-en." >> /dev/stderr
    return 1
  fi

  # Xixa
  allfstype
  return 0
}

